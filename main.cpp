#include <iostream>
#include <map>
#include <fstream>
#include <bitset>
#include <cmath>

#define DONT_CARE -1
#define REGISTER_FILE_SIZE 32
using namespace std;

class Instruction;
int indexOfInstruction(int first,int last,int instruction);
int createNumber(int first,int last){
    unsigned int sum =0;
    for (int i = first ; i <= last; i++){
        sum  = sum + (unsigned)pow(2,i);
    }
    return sum;
}
int indexOfInstruction(int first, int last,int instruction) {
    unsigned int sum = createNumber(first,last) ;
    return (instruction&sum)>>(first-1);
}
int getOpCode(int instruction) {
    return indexOfInstruction(26,31,instruction);
}

int decodeOpCode(int instruction){
    int opCode = getOpCode(instruction);
    if(opCode==0){
        return  0;//R_Type
    }
    else if(opCode==35 || opCode ==43){
        return 1;//I_Type
    }
    else if(opCode == 2 || opCode==3){
        return 2;//J_Type
    }
    return -1;//incorrect instruction
}


// Register File




class RegisterFile{

private:
    int registers[REGISTER_FILE_SIZE];
public:
    RegisterFile();
    void setWriteRegister(int number);

    void setData(int registerNumber , int data);
    int getData(int registerNumber);
};
RegisterFile::RegisterFile() {
    fill_n(registers,REGISTER_FILE_SIZE,0);
}
void RegisterFile::setData(int registerNumber, int data) {
    this->registers[registerNumber]=data;
    return;
}
int RegisterFile::getData(int registerNumber){
    return this->registers[registerNumber];
}




// Instruction
class Instruction{
private:
    unsigned  int instruction;
public:
    void setInstruction(string instruction);
    int getInstruction();
};
int Instruction ::getInstruction() {
    return this->instruction;
}

void Instruction ::setInstruction(string instruction) {
    this->instruction = stoi(instruction,nullptr,2);
}

//Pipeline Register




//main Control Unit
class MainControlUnit{
private:
    int  ALUsrc    ;
    int  AluOp1    ;
    int  AluOp2    ;
    int  MemRead   ;
    int  MemWrite  ;
    int  RegWrite  ;
    int  MemToReg  ;
    int  RegDst    ;
    int  Branch    ;
    int  Jump      ;
public:
    void setSignals(int opCode );
    int  getSignal(string signalName);
};

int MainControlUnit::getSignal(string signalName) {
        if( "RegDst"==signalName)
            return this->RegDst;
        if( "AluOp1" ==signalName  )
            return this->AluOp1;
        if( "AluOp2"   ==signalName)
            return this->AluOp2;
        if( "AluSrc"   ==signalName)
            return this->ALUsrc;
        if( "Branch"    ==signalName)
            return this->Branch;
        if( "MemToReg"   ==signalName )
            return this->MemToReg;
        if( "MemRead"    ==signalName)
            return this->MemRead;
        if( "MemWrite"    ==signalName)
            return this->MemWrite;
        if( "Jump"   ==signalName )
            return this->Jump;
        return DONT_CARE;
}
void MainControlUnit:: setSignals(int instruction){
    int opCode = getOpCode(instruction);
    int type = decodeOpCode(opCode);
    map<string,int> signals;
    if (type==0){
       	this->ALUsrc=0;
       	this->AluOp1=0;
       	this->AluOp2=1;
       	this->MemRead=0;
       	this->MemWrite=0;
       	this->RegWrite=1;
       	this->MemToReg=0;
       	this->RegDst=1;
       	this->Branch=0;
       	this->Jump=0;
    }
    else if (type ==1 ){
        if (opCode ==35) {

            this->ALUsrc=1;
           	this->AluOp1=0;
           	this->AluOp2=0;
           	this->MemRead=1;
           	this->MemWrite=0;
           	this->RegWrite=1;
           	this->MemToReg=1;
           	this->RegDst=0;
           	this->Branch=0;
           	this->Jump=0;
        }
        else if (opCode ==43){
       	    this->ALUsrc=1;
       	this->AluOp1=0;
       	this->AluOp2=0;
       	this->MemRead=0;
       	this->MemWrite=1;
       	this->RegWrite=0;
       	this->MemToReg=DONT_CARE;
       	this->RegDst=DONT_CARE;
       	this->Branch=0;
       	this->Jump=0;
        }
        else if(opCode ==4 || opCode ==5){
            this->ALUsrc=0;
       	this->AluOp1=1;
       	this->AluOp2=0;
       	this->MemRead=0;
       	this->MemWrite=0;
       	this->RegWrite=0;
       	this->MemToReg=DONT_CARE;
       	this->RegDst=DONT_CARE;
       	this->Branch=1;
       	this->Jump=0;
        }
    }
    else if (type ==2 ){
            this->ALUsrc=DONT_CARE;
       	this->AluOp1=DONT_CARE;
       	this->AluOp2=DONT_CARE;
       	this->MemRead=0;
       	this->MemWrite=0;
       	this->RegWrite=0;
       	this->MemToReg=DONT_CARE;
       	this->RegDst=DONT_CARE;
       	this->Branch=0;
       	this->Jump = 1;
    }
    return;
}



//alu Control signals
int aluControl(int function , int aluOp1 ,int aluOp2){
    if(aluOp2==0 && aluOp2==0) {
        return 2;//010 addi , lw , sw
    }
    else if (aluOp2==1 && aluOp1==DONT_CARE  ){
        return 6;//110 beq , bne
    }
    else if(aluOp2==DONT_CARE  && aluOp1==0){
        if(function== 32  ){
            return 2;//010 add
        }
        else if (function ==34){
            return 6;//110 sub
        }
        else if (function==36){
            return 0;//000 and
        }
        else if (function==37){
            return 1;//001 or
        }
        else if(function==42){
            return 7;//111 slt
        }
    }
    return -1;//op code or function not correct
}



//ALU
class Alu {
private:
    int zero ;
    int result ;
public:
    Alu();
   void Opperator(int opperand1, int opperand2 , int aluControlSingal);
   int getResult();
   int getZero();
};
void Alu::Opperator(int opperand1 , int opperand2, int aluControlSignal){
    this->zero =0;
    if (aluControlSignal==0) {
        this->result= opperand1&opperand2;
    }
    else if(aluControlSignal==1){
        this->result= opperand1|opperand2;
    }
    else if(aluControlSignal==2){
        this->result= opperand1+opperand2;
    }
    else if(aluControlSignal==6){
        this->result=  opperand1-opperand2;
        this->zero = opperand1==opperand2?1:0;
    }
    else if(aluControlSignal==7){
        this->result  =  opperand1<opperand2;
    }
}
int Alu::getResult() {
    return this->result;
}
int Alu::getZero() {
    return  this->zero;
}



//shiftLeftTwo
int shiftLeftTwo(int num ){
    return num<<2;
}




//Pipline Registers
struct IdEx{
    MainControlUnit mainControlUnit;
    int instruction;
    int readregister1;
    int readregister2;
    int pcSource;
    int registerRt;
    int registerRs;
};
struct IfId{
    int instruction;
    int pcsource;
};
int ExMe[6],MeWb[5];




//main
int main() {
    cout<<"fuck"<<endl;
    bitset<32> x(65535);
    cout<<x<<endl;
    bitset<32> y(-65535);
    cout<<y<<endl;
    cin>>x;
    Instruction instructions[128];
    IdEx idEx;
    IfId ifId;
    int writeRegister;
    int readRegister1;
    int readRegister2;
    ifstream file ("/home/sh/CLionProjects/untitled/myop.txt");
    int i = 0 ;
    cout<<"fuck1"<<endl;
    if(!file.is_open()){
        cout<<"file not exist"<<endl;
        return 1;
    }
    cout<<"fuck1.5"<<endl;
    string instruction;
    while (getline(file,instruction) && i<128){
        instructions[i].setInstruction(instruction);
        cout<<instructions[i].getInstruction();
        i++;
        cout<<i<<endl;
    }
    cout<<"fuck2"<<endl;
    MainControlUnit mainControlUnit;
    for (int pc = 0 ; pc<128*4;pc=pc+2){//2 is negative edge and 4 is pasetive edge
        if ( pc%2==0){
            idEx.mainControlUnit = mainControlUnit;
            idEx.instruction = ifId.instruction;
            idEx.pcSource = ifId.pcsource;
            idEx.readregister1 = readRegister1;
            idEx.readregister2 = readRegister2;
            idEx.registerRs=indexOfInstruction(11,15,ifId.instruction);
            idEx.registerRt=indexOfInstruction(16,20,ifId.instruction);
            ifId.instruction = instructions[pc/4].getInstruction();
            ifId.pcsource=pc+4;
        }
        else if (pc%2==2){
            mainControlUnit.setSignals(ifId.instruction);
            readRegister1=indexOfInstruction(21,25,instructions[pc/4].getInstruction());
            readRegister2=indexOfInstruction(16,20,instructions[pc/4].getInstruction());
            if(mainControlUnit.getSignal("RegDst")){
                 writeRegister = indexOfInstruction(11,15,instructions[pc/4].getInstruction());
            }
            else{
                writeRegister = indexOfInstruction(16,20,instructions[pc/4].getInstruction());
            }
        }
    }
    cout << "Hello, World!" << std::endl;
    return 0;
}
